from src.getRwords import *
from copy import deepcopy
import re
rWords=getRwords()
error=""
s_list=[]
c_list=[]
#CheckingVariableName
def C_varName(x):
    valid=True
    for temp in x:
        if not(temp.isalpha()) and not(temp.isdigit()) and temp!='_':
            valid=False
    return valid


def C_module(x):
    valid=True
    if not(x[0].isalpha()):
        false
    for temp in x:
        if not(temp.isalpha()) and not(temp.isdigit()) and temp!='.':
            valid=False
    return valid

#Converting character const and string const
def tokenize_string(sentence):
    global error
    global s_list
    global c_list
    s=0
    e=0
    my_len=len(sentence)
    if len(error)!=0:
        return error
    elif "\"" in sentence or "\'" in sentence:
        for x in range (0,my_len):
            if sentence[x]=='\"' or sentence[x]=='\'':
                s=x
                e=s
                for y in range (x+1,my_len):
                    if sentence[y]==sentence[s] and sentence[y-1]!="\\":
                        e=y+1
                        break
            if e!=s:
                if e==len(sentence)+1:
                    error=error+"Expected \" or \' of a string/character. "
                    return error
                else:
                    if e-s<=3:
                        c_list.append(sentence[s:e])
                        sentence = sentence[0:s]+"<char_const>"+sentence[e:]

                    else:
                        s_list.append(sentence[s:e])
                        sentence = sentence[0:s]+"<string_const>"+sentence[e:]
                    return tokenize_string(sentence)

        #print list(sentence)
    else:
        return sentence




def Analyzer(x):
    global rWords
    global error
    if x!='' or x!="":
        if x in rWords['reserved']:
            return x
        else:
            if x[0].isalpha():
                if C_varName(x):
                    return "<id>"
                else:
                    if C_module(x):
                        return "<module_path>"
                    else:
                        error=error+"There is an <<INVALID INPUT>>. "
                        return "<<INVALID INPUT>>"

            elif x=="Tama" or x=="Mali":
                return '<boolean_const>'
            else:
                try:
                    int(x)
                    return '<int_lit>'
                except ValueError:
                    try:
                        float(x)
                        return '<float_const>'
                    except ValueError:
                        error=error+"There is an <<INVALID INPUT>>. "
                        return '<<INVALID INPUT>>'
    else:
        return x



def L_Analyze(temp):
    global error
    global c_list
    global s_list
    my_return={}
    temp = tokenize_string(temp)
    try:
        if temp!="None":
            temp=re.split(" |\n",temp)
            count=0
            mylen=len(temp)

            while count in range (0,mylen):
                if temp[count]=="":
                    del temp[count]
                    mylen-=1
                else:
                    count+=1
            d=[]
            for x in temp:
                if x!="<string_const>" and x!="<char_const>":
                    d.append(x)
                else:
                    if x=="<string_const>":
                        d.append(s_list.pop(0))
                    else:
                        d.append(c_list.pop(0))
            my_return["raw"]=d
        else:
            error=error+"Expected \" or \' of a string/character. "
    except TypeError:
        error=error+"Expected \" or \' of a string/character. "
        my_return["error"] = error

    if len(error)==0:
        tokenized=[]
        for x in temp:
            if x!="<string_const>" and x!="<char_const>":
                if x in ['.',',','+','-','/','*','\n','\t','(',')','[',']','~','{','}']:
                    tokenized.append(x)
                else:
                    tokenized.append(Analyzer(x))
            else:
                tokenized.append(x)
        my_return["tokenized"]=tokenized
        my_return["error"] = error
	my_return["error"]=error
    if len(error)==0:
        for x in range(len(my_return["tokenized"])-1,-1,-1):
            try:
                if my_return["tokenized"][x]=="<id>":
                    if my_return["tokenized"][x+1]=="(" or my_return["tokenized"][x-1]=="kakayahang":
                        my_return["tokenized"][x]="<funct_id>"
                    elif my_return["tokenized"][x+1]=="~":
                        my_return["tokenized"][x]="<obj_id>"
                    if my_return["tokenized"][x-1]=="bagong" or my_return["tokenized"][x-1]=="klase":
                        my_return["tokenized"][x]="<class_id>"
                    if my_return["tokenized"][x+1]=="ay" and my_return["tokenized"][x+2]=="bagong":
                        my_return["tokenized"][x]="<obj_id>"
                    if my_return["tokenized"][x-1]=="isama":
                        my_return["tokenized"][x]="<module_path>"
            except:
                pass
	my_return["error"]=error
    return my_return

'''
#TESTING AREA
f=file('test','r')
L_Analyze(f.read())
#print tokenize_string(f.read())
f.close()
'''
