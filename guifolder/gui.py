import os
from Tkinter import *
from ScrolledText import *
from tkFileDialog import *

class Application(object):
	def __init__(self, master, **kwargs):
		"""Initializes all values for Tkinter window"""
		self.master = master
		self.saved = False
		self.filename = ""
		
		"""Initializes the window to 1024x768 pixels"""
		self.master.geometry('{}x{}'.format(1024, 768))

		"""Initializes Button for Loading File"""
		self.load = Button(self.master, text="Load", relief=FLAT, command=self.loadfile)
		self.load.pack()
		self.load.place(x=10, y=10)

		"""Initializes Button for Saving File"""
		self.save = Button(self.master, text="Save", relief=FLAT, command=self.savefile)
		self.save.pack()
		self.save.place(x=70, y=10)

		"""Initializes Button for Running File"""
		self.run = Button(self.master, text="RUN", command=self.runcode)
		self.run.pack()
		self.run.place(x=10, y=505)

		"""Initializes the Editor text area"""
		self.editor = ScrolledText(master=self.master, width=1024, height=30)
		self.editor.pack(padx=10, pady=40)

		"""Initializes the Logger text area"""
		self.logger = ScrolledText(master=self.master, width=1024, height=25)
		self.logger.pack(padx=10, pady=5)
		self.logger.config(state=DISABLED)

		self.extra = Button(self.master, text="EXTRA", command=self.errormessage)
		self.extra.pack()
		self.extra.place(x=130, y=10)

	def loadfile(self):
		"""Opens a file dialog and loads the file into the text area.
		   Is triggered when clicking Load button. """
		self.filename = askopenfilename()
		afile = open(self.filename, 'r+')	
		if afile != None:	
			self.editor.insert(INSERT, afile.read())
			afile.close()

	def savefile(self):
		"""Opens a file dialog and saves the current content of text area to a file.
		   Is triggered when clicking Save button."""
		afile = asksaveasfile(mode='w')
		
		if afile != None:
			afile.write(self.editor.get('1.0', END))
			afile.close()

	def errormessage(self, message=None):
		"""Temporarily creates an error message 'ERROR'.
		   Is triggered when EXTRA is pressed. """
		self.logger.config(state=NORMAL)
		self.logger.delete('1.0', END)
		if message != None:
			self.logger.insert(INSERT, message)
		else:
			self.logger.insert(INSERT, "ERROR")
		self.logger.config(state=DISABLED)

	def runcode(self):
		"""Opens up a terminal and runs a particular python code.
		   Is triggered when Run is pressed."""
		runterminal = "gnome-terminal -e 'python -i ../TEST2.py " + self.filename + "'"
		os.system(runterminal)


"""Initialize Tkinter Application"""
root = Tk()
app = Application(root)

"""Open and Run the Tkinter Application"""
root.mainloop()







"""EXTRA: Used to Force destroy if application cannot be destroyed"""
	#root.destroy()
