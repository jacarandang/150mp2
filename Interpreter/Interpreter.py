from Expression import Expression
from BoolExpr import BoolExpr

class Class:

    def __init__(self):
        pass

class Function:
    def __init__(self, arg_list):
        pass
    def addStatement(self, statement):
        pass
    def execute(self, arg_dict):
        pass

class Interpreter:

    def __init__(self):
        self.var_dict = {}
        self.func_dict = {}
        self.object_dict = {}
        self.error = None
        self.error_str = None
	"""Initial values for Conditional Statements"""
        self.open_cond = False
        self.open_cont_cond = False

    def assg(self, raw, token):
        name = raw[1]
        expr = Expression(raw[3:], token[3:], self.var_dict, self.func_dict, self.object_dict)
        val = expr.start()
        self.var_dict[name] = val

    def print_s(self, raw, token):
        #print self.var_dict
        for i in xrange(1, len(raw), 2):
            if token[i] in ["<string_const>", "<char_const>", "<float_const>", "<int_lit>", "<boolean_const>"]:
                if token[i]=="<string_const>" or token[i]=="<char_const>":
                    a = raw[i][1:len(raw[i])-1]
                    if a == "\n":
                        print ""
                    else:
                        print a ,
                else:
                    print raw[i] ,
            elif token[i] == "func_call":
                print "function call",
            elif token[i] == "<id>":
                print self.var_dict[raw[i]][1:-1]

    def loop(self, raw, tok):
        while(True):
            blexpr_raw = []
            blexpr_tok = []
            st_idx = None
            for i in xrange(1, len(raw)):
                if tok[i] == ',':
                    st_idx = i
                    break
                blexpr_raw.append(raw[i])
                blexpr_tok.append(tok[i])
            blexpr = BoolExpr(blexpr_raw, blexpr_tok, self.var_dict, self.func_dict, self.object_dict)
            if not blexpr.start():
                break
            coms_raw = []
            coms_tok = []
            st_idx += 3
            while True:
                if tok[st_idx] == "}":
                    break
                tmp_raw = []
                tmp_tok = []
                count = 0
                for i in xrange(st_idx, len(raw)):
                    if(tok[i] == "{"):
                        count += 1
                    if(tok[i] == "." and count == 0):
                        st_idx = i + 1
                        break
                    if tok[i] == "}":
                        count -= 1
                    tmp_raw.append(raw[i])
                    tmp_tok.append(tok[i])
                coms_raw.append(tmp_raw)
                coms_tok.append(tmp_tok)
            for i in range(len(coms_raw)):
                com = coms_raw[i]
                if com[0] == 'Ang':
                    self.assg(coms_raw[i], coms_tok[i])
                elif com[0] == 'Ipakita':
                    self.print_s(coms_raw[i], coms_tok[i])
                elif com[0] == 'Si':
                    self.scan_s(coms_raw[i], coms_tok[i])
                elif com[0] == "Habang":
                    self.loop(coms_raw[i], coms_tok[i])


    def conditional(self, raw, tok):
        print 'ENTER CONDITIONAL!'
        print 'raw = ', raw
        print 'tok = ', tok

	"""Initialize lists for raw and tok boolean expressions"""
        boolraw = []
        booltok = []

	"""Get Boolean expression from raw"""
        index = 1
        while index < len(raw):
            if raw[index] != ',':
                boolraw.append(raw[index])
            else:
                break
            index = index + 1

	"""Get Boolean expression from tok"""
        index = 1
        while index < len(tok):
            if tok[index] != ',':
                booltok.append(tok[index])
            else:
                break
            index = index + 1

	"""Check if expression has 'naman'"""
        print 'boolraw', boolraw
        print 'booltok', booltok

        if boolraw[len(boolraw)-1] == "naman" and booltok[len(booltok)-1] == "naman":
            self.open_cont_cond = True
            boolraw.remove('naman')
            booltok.remove('naman')
        else:
            self.open_cond = True

        print 'boolraw', boolraw
        print 'booltok', booltok

        print 'open_cond = ', self.open_cond
        print 'open_cont_cond = ', self.open_cont_cond

	"""Create the boolean expression and evaluate it"""
        boolexper = BoolExpr(boolraw, booltok, self.var_dict, self.func_dict, self.object_dict)
        print "here"

        if boolexper.start() == None:
            print 'RUNTIME ERROR'
        elif boolexper.start():
            print 'boolean true'
        elif not boolexper.start():
            print 'boolean false'

        print "or here"
        print 'ENTER CONDITIONAL!'
        print 'raw = ', raw
        print 'tok = ', tok

        """Initialize lists for raw and tok boolean expressions"""
        boolraw = []
        booltok = []

    	"""Get Boolean expression from raw"""
        index = 1
        while index < len(raw):
            if raw[index] != ',':
                boolraw.append(raw[index])
            else:
                break
    		index = index + 1

    	"""Get Boolean expression from tok"""
        index = 1
        while index < len(tok):
            if tok[index] != ',':
                booltok.append(tok[index])
            else:
                break
            index = index + 1

    	"""Check if expression has 'naman'"""
        print 'boolraw', boolraw
        print 'booltok', booltok

        if boolraw[len(boolraw)-1] == "naman" and booltok[len(booltok)-1] == "naman":
            self.open_cont_cond = True
            boolraw.remove('naman')
            booltok.remove('naman')
        else:
            self.open_cond = True

        print 'boolraw', boolraw
        print 'booltok', booltok

        print 'open_cond = ', self.open_cond
        print 'open_cont_cond = ', self.open_cont_cond


        """Create the boolean expression and evaluate it"""
        boolexper = BoolExpr(boolraw, booltok, self.var_dict, self.func_dict, self.object_dict)
        if boolexper.start() == None:
            print 'RUNTIME ERROR'
        elif boolexper.start():
            print 'boolean true'
        elif not boolexper.start():
            print 'boolean false'





    def scan_s(self, raw, tok):
        inp = raw_input()
        name = raw[1]
        self.var_dict[name] = inp

    def typecast(self, id, ):
        pass

    def execute(self, com_raw, com_tok):
        if com_raw[0] == "Ang":
            self.assg(com_raw, com_tok)
        elif com_raw[0] == "Habang":
            self.loop(com_raw, com_tok)
        elif com_raw[0] == "Si":
            self.scan_s(com_raw, com_tok)
        elif com_raw[0] == "Ipakita":
            self.print_s(com_raw, com_tok)
        elif com_raw[0] == "Kung":
            self.conditional(com_raw, com_tok)
