class Expression:

    def __init__(self, raw, token, var_dict, func_dict, object_dict):
        self.raw = raw
        self.token = token
        self.l = len(self.raw)
        self.idx = 0
        self.var_dict = var_dict
        self.func_dict = func_dict
        self.object_dict = object_dict

    def get_current(self):
        #print 'self.raw = ', self.raw
        #print 'get_current: ', self.idx
        return (self.raw[self.idx], self.token[self.idx])

    def set_next(self):
        self.idx += 1
        return self.idx

    def start(self):
        a = self.get_current()
        if a[1] in ["numerong", "palutang", "titik", "lubid", "class_id"]:
            # print "here at typecast", self.var_dict
            raw_expr = self.raw[1:len(self.raw) - 1]
            tok_expr = self.token[1:len(self.raw) - 1]
            # print raw_expr, tok_expr, "are the expression"
            # print self.var_dict, "before expression"
            expr = Expression(raw_expr, tok_expr, self.var_dict, self.func_dict, self.object_dict)
            val = expr.start()
            # print val, "final value"
            #error handling
            if a[1] == "numerong":
                return int(val)
            elif a[1] == "palutang":
                return float(val)
            elif a[1] == "titik":
                return str(val)[0]
            elif a[1] == "lubid":
                return str(val)
            return False
        #handle typecast
        val1 = self.term()
        if self.idx >= self.l:
            return val1
        opt = self.get_current()
        if opt[0] not in ["+", "-"]:
            return val1
        self.set_next()
        if self.idx >= self.l:
            return val1
        val2 = self.start()
        if opt[1] == '+':
            return val1+val2
        elif opt[1] == '-':
            return val1-val2

    def term(self):
        val = self.factor()
        if self.idx >= self.l:
            return val
        opt = self.get_current()
        if opt[1] not in ['*', '/']:
            return val
        self.set_next()
        val2 = self.term()
        if opt[1] == '*':
            return val*val2
        elif opt[1] == '/':
            return val/val2

    def factor(self):
        val = None
        cur = self.get_current()
        # print cur, "in factor"
        if cur[1] == '<id>':
	    #print '<id>'
            val = self.var_dict[cur[0]]
        elif cur[1] == '<string_const>':
            val = cur[0]
        elif cur[1] == '<char_const>':
            val = cur[0]
        elif cur[1] == '<float_const>':
            val = float(cur[0])
        elif cur[1] == '<int_lit>':
            val = int(cur[0])
        elif cur[1] == '<func_call>':
            val = 1
        elif cur[1] == '<boolean_const>':
            val = True
        elif cur[1] == '(':
	        #print 'else'
            self.set_next()
            val = self.start()
            #supposedly )
        self.set_next()
	#print 'setnext:', self.idx
        return val
