inp = open("table2.txt", "r")
out1 = open("4.txt", "w")
out2 = open("5.txt", "w")
out3 = open("6.txt", "w")

line = inp.readline()
tabCount = 0
part = 0

while line:
  out1.write("[\'")
  out2.write("[\'")
  out3.write("[\'")

  for c in line:
    if(c == ' '):
      t = True
    elif(ord(c) == 9):
      if(part == 0):
        out1.write("\'")
        out1.write(":")
        out1.write(str(tabCount))
        out1.write(",\'")
      elif(part == 1):
        out2.write("\'")
        out2.write(":")
        out2.write(str(tabCount))
        out2.write(",\'")
      elif(part == 2):
        out3.write("\'")
        out3.write(":")
        out3.write(str(tabCount))
        out3.write(",\'")

      print tabCount,
      tabCount += 1
      if(tabCount == 1):
        out1.write("],\n")
        part = 1
        print "part1"
      elif(tabCount == 58):
        out2.write("],\n")
        part = 2
        print "part2"

    elif(ord(c) == 10):
      out3.write("\'],\n")

    else:
      if(part == 0):
        out1.write(c)
      elif(part == 1):
        out2.write(c)
      elif(part == 2):
        out3.write(c)

  part = 0
  tabCount = 0
  line = inp.readline()

inp.close()
out1.close()
out2.close()
out3.close()
