import re, sys
from LexicalAnalyzer.L_Analyzer import *
from Interpreter import Interpreter
from new_lr import *
from copy import deepcopy
paren = 0;
temp = ""
openparen = "{"
closeparen = "}"
my_return = []
code = []
i = 0
sep = '#'
errors=0

if len(sys.argv) > 1:
	filename = sys.argv[1]
else:
	filename = "kahitano"

with open(filename) as openfileobject:
	#print list(openfileobject)
	for line in openfileobject:
		i += 1
		#REMOVE ALL INSTANCE OF  "%string here" IN THE LINE
		result = re.sub(r'\'.*?\'', '', line)
		#REMOVE ALL INSTANCE OF COMMENT IN THE LINE
		#result = re.sub(r'#.*?', '', results)
		#result = result.split("#")
		#result = result.split(sep,1)[0]
		#result = result.split('#',1)[0]
		result,sep,tail = result.partition('#')
		#print result
		if len(result) == 0 :
			#print 'THIS LINE IS A COMMENT'
			pass
		else:
			line = result
		#COUNT ALL {
			if len(line) != 1:
				paren = paren + result.count('{')
				#print paren
				#REMOVE ALL WHITE SPACE BEFORE FIRST NON-WHITE
				while (line[0] == '\t' or line[0] == ' ') :
					line = line[1:len(line)]
				#REMOVE ALL WHITE SPACE AFTER LAST NON-WHITE
				while (line[len(line)-1] == '\n' or line[len(line)-1] == ' ' or line[len(line)-1] == '\t'):
					line = line[0:len(line)-1]
					#ALL ARE WHITE SPACE
					if len(line) == 0:
						break
				temp = temp + line + " "
				#COUNT ALL }
				paren = paren - result.count('}')
			#	print paren
				if	paren == -1:
					print "Error! Extra closing parenthesis at line " + str(i)
					break
				elif paren == 0:
					temp = temp[0:len(temp)-1] #remove white space in last char, just format of output
					temp2 = L_Analyze(temp)
					#print temp2
					temp3=deepcopy(temp2)
					if len(temp2["error"]) == 0 :
						lr=LR()
						lr.parse(temp2)
						if len(lr.r_Result()["error"])==0:
							code.append(temp3)
						else:
							code=[]
							errors+=1
							print temp2["error"]
					else:
						errors+=1
						print temp2["error"]
						break
					temp = ""

	#if paren != 0:
	#	print "Error! Missing closing parenthesis!"
i = Interpreter.Interpreter()
if errors==0:
	for c in code:
		i.execute(c["raw"], c["tokenized"])
