Language Name: Titik o
TTh Ruby
Carandang
Datoc
Domingo
Ibasco
Otsuka
Sangun

Sample codes located at ./codes

In order to run
python test.py <code>

To run GUI
python gui.py (The run feature runs in Ubuntu)

Lexical Analyzer at ./LexicalAnalyzer
Interpreter at ./Interpreter
LR parser at new_lr.py
